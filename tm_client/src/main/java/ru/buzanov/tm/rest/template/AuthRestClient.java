package ru.buzanov.tm.rest.template;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.buzanov.tm.dto.UserDTO;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Component
public class AuthRestClient {
    @NotNull
    final String adress = "http://localhost:8080/rest/";

    public String auth(@NotNull final String login, @NotNull final String password) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("login", login);
        headers.add("password", password);
        HttpEntity<String> request = new HttpEntity<String>(headers);
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(new URI(adress + "auth"), request, String.class);
            HttpHeaders respheaders = response.getHeaders();

             return respheaders.get("Set-Cookie").toString();
//            restTemplate.put(new URI(adress + "auth"), userDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String profile(@NotNull final String header) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Set-Cookie", header);
        HttpEntity<String> request = new HttpEntity<String>(headers);
        try {
            ResponseEntity<UserDTO> response = restTemplate.exchange(new URI(adress + "profile"), HttpMethod.GET, request, UserDTO.class);


            return response.getBody().getLogin()+" "+response.getBody().getName();
//            restTemplate.put(new URI(adress + "auth"), userDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
//
//    public void mergeAll(final Collection<UserDTO> users) {
//        @NotNull final RestTemplate restTemplate = new RestTemplate();
//        try {
//            restTemplate.put(new URI(adress + "mergeAll/"), users);
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public UserDTO findOne(final String id) {
//        @NotNull final RestTemplate restTemplate = new RestTemplate();
//        try {
//            return restTemplate.getForObject(new URI(adress + "find/" + id), UserDTO.class);
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    public List<UserDTO> findAll() {
//        @NotNull final RestTemplate restTemplate = new RestTemplate();
//        try {
//            final UserDTO[] userDTOS = restTemplate.getForObject(new URI(adress + "findAll"), UserDTO[].class);
//            if (userDTOS == null)
//                return null;
//            return Arrays.asList(userDTOS);
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    public void removeOne(final String id) {
//        @NotNull final RestTemplate restTemplate = new RestTemplate();
//        try {
//            restTemplate.delete(new URI(adress + "remove/" + id));
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void removeAll() {
//        @NotNull final RestTemplate restTemplate = new RestTemplate();
//        try {
//            restTemplate.delete(new URI(adress + "removeAll"));
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
//    }

}
