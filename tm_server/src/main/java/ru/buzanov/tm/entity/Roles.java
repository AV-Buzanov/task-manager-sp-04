package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.enumerated.RoleType;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_roles")
public class Roles extends AbstractEntity {
    @Nullable
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Enumerated(value = EnumType.STRING)
    private RoleType roleType;

    public Roles(@Nullable User user, RoleType roleType) {
        this.user = user;
        this.roleType = roleType;
    }
}
