package ru.buzanov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.buzanov.tm.enumerated.Status;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractWBS extends AbstractDto {
    private int count;
    @Nullable private String name;
    @Nullable private String userId;
    @Nullable private Status status = Status.PLANNED;
    @Nullable private Date createDate = new Date(System.currentTimeMillis());
    @Nullable private Date startDate;
    @Nullable private Date finishDate;
    @Nullable private String description;
}