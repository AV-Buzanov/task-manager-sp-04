package ru.buzanov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.buzanov.tm.configuration.AppConfig;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.enumerated.Status;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Category(DataTests.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class TaskServiceTest extends Assert {
    @Nullable
    @Resource
    private TaskService taskService;
    @Nullable
    @Resource
    private UserService userService;
    private UserDTO user;
    private UserDTO user2;

    @Before
    public void setUp() throws Exception {
        user = new UserDTO();
        user.setLogin("login");
        user.setPasswordHash("123456");
        user.getRoles().add(RoleType.USER);
        userService.load(user);
        user2 = new UserDTO();
        user2.setLogin("login2");
        user2.setPasswordHash("123456");
        user2.getRoles().add(RoleType.USER);
        userService.load(user2);
    }

    @After
    public void tearDown() throws Exception {
        userService.remove(user.getId());
        userService.remove(user2.getId());
    }

    @Test
    public void testTaskCreate() throws Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        taskService.load(user.getId(), task);
        @Nullable final TaskDTO testTask = taskService.findOne(user.getId(), task.getId());
        assertNotNull(testTask);
        assertEquals("not equals id", task.getId(), testTask.getId());
        assertEquals("not equals userId", user.getId(), testTask.getUserId());
        assertEquals("not equals Name", task.getName(), testTask.getName());
        assertEquals("not equals createDate", task.getCreateDate().getTime(), testTask.getCreateDate().getTime());
        assertEquals("not equals status", Status.PLANNED, testTask.getStatus());
    }

    @Test(expected = Exception.class)
    public void testTaskCreateExistName() throws Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        taskService.load(user.getId(), task);
        @NotNull final TaskDTO testTask = new TaskDTO();
        testTask.setName("Name");
        taskService.load(user.getId(), testTask);
    }

    @Test
    public void testTaskMerge() throws Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        task.setDescription("description");
        task.setStartDate((new Date(System.currentTimeMillis())));
        task.setFinishDate((new Date(System.currentTimeMillis())));
        taskService.load(user.getId(), task);
        task.setName("newName");
        task.setDescription("newDescription");
        task.setStartDate((new Date(System.currentTimeMillis())));
        task.setFinishDate((new Date(System.currentTimeMillis())));
        task.setStatus(Status.IN_PROGRESS);
        taskService.merge(user.getId(), task.getId(), task);
        @Nullable final TaskDTO testTask = taskService.findOne(user.getId(), task.getId());
        checkEquals(testTask, task);
    }

    @Test
    public void testTaskFind() throws Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        task.setDescription("description");
        task.setStartDate((new Date(System.currentTimeMillis())));
        task.setFinishDate((new Date(System.currentTimeMillis())));
        task.setStatus(Status.PLANNED);
        taskService.load(user.getId(), task);
        @Nullable final TaskDTO testTask = taskService.findOne(user.getId(), task.getId());
        checkEquals(testTask, task);
    }

    @Test
    public void testTaskFindAllSize() throws Exception {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("Name");
        taskService.load(user.getId(), task);
        task = new TaskDTO();
        task.setName("Name2");
        taskService.load(user.getId(), task);
        task = new TaskDTO();
        task.setName("Name3");
        taskService.load(user.getId(), task);
        @Nullable final List<TaskDTO> list = new ArrayList<>(taskService.findAll(user.getId()));
        assertNotNull(list);
        assertEquals("size error", 3, list.size());
    }

    @Test
    public void testTaskRemove() throws Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        taskService.load(user.getId(), task);
        taskService.remove(user.getId(), task.getId());
        @Nullable final TaskDTO testTask = taskService.findOne(user.getId(), task.getId());
        assertNull(testTask);
    }

    @Test
    public void testTaskFindWithWrongUser() throws Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        taskService.load(user.getId(), task);
        @Nullable final TaskDTO testTask = taskService.findOne(user2.getId(), task.getId());
        assertNull(testTask);
    }

    @Test
    public void testTaskRemoveWithWrongUser() throws Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        taskService.load(user.getId(), task);
        taskService.remove(user2.getId(), task.getId());
        assertNotNull(taskService.findOne(user.getId(), task.getId()));
    }

    private void checkEquals(@Nullable final TaskDTO testTask, @Nullable final TaskDTO task) {
        assertNotNull(testTask);
        assertNotNull(testTask.getStartDate());
        assertNotNull(testTask.getFinishDate());
        assertNotNull(task);
        assertNotNull(task.getStartDate());
        assertNotNull(task.getFinishDate());
        assertEquals("not equals id", task.getId(), testTask.getId());
        assertEquals("not equals userId", user.getId(), testTask.getUserId());
        assertEquals("not equals Name", task.getName(), testTask.getName());
        assertEquals("not equals desc", task.getDescription(), testTask.getDescription());
        assertEquals("not equals createDate", task.getCreateDate().getTime(), testTask.getCreateDate().getTime());
        assertEquals("not equals startDate", task.getStartDate().getTime(), testTask.getStartDate().getTime());
        assertEquals("not equals finishDate", task.getFinishDate().getTime(), testTask.getFinishDate().getTime());
        assertEquals("not equals status", task.getStatus(), testTask.getStatus());
    }
}